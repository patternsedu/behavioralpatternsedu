package Iterator.iterator;

public interface IterableUICollection {
	UIIterator getIterator();
}
