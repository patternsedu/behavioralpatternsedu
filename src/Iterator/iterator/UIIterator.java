package Iterator.iterator;

import Iterator.widgets.UIComponent;

public interface UIIterator {
	UIComponent getNext();
	boolean hasMore();
}
