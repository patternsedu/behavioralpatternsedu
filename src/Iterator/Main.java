package Iterator;

import Iterator.iterator.ContentControlIterator;
import Iterator.iterator.UIIterator;
import Iterator.widgets.*;

public class Main {

	public static void main(String[] args) {
		
		// создаём дерево
		CompositeControl mainWin = new MainWindow();
		CompositeControl frame1 = new CompositeControl()
				.add(new Label("Login")).add(new Button("OK"));
		CompositeControl frame2 = new CompositeControl()
				.add(new Label("Password")).add(new Button("Verify"));
		CompositeControl frame3 = new CompositeControl()
				.add(new Button("Print"));
		mainWin.add(frame1).add(frame2).add(frame3);
		mainWin.draw();
		
		System.out.println("Итератор всего дерева, включая композиты");
		UIIterator iter = mainWin.getIterator();
		while(iter.hasMore()) {
			System.out.println(iter.getNext());
		}
			
		System.out.println("Итератор только для ContentControl");
		ContentControlIterator iterContent = mainWin.getContentIterator();
		while(iterContent.hasMore()) {
			System.out.println(iterContent.getNext().getText());
		}
	}
}
