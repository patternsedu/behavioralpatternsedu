package Visitor.graph;

public abstract  class AbstractGOFactory {
	public abstract Point createPoint();
	
	public abstract Circle createCircle();
}
