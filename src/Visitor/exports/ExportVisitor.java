package Visitor.exports;

import Visitor.graph.Circle;
import Visitor.graph.Point;

public interface ExportVisitor {
	void exportPoint(Point p);
	void exportCircle(Circle c);
}
