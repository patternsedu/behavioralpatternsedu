package Visitor;

import Visitor.graph.AbstractGOFactory;
import Visitor.graph.ColorGOFactory;
import Visitor.graph.Scene;

public class Main {

	public static void main(String[] args) {
		AbstractGOFactory gof = new ColorGOFactory();
		gof.createPoint().setColor("red");
		gof.createPoint().setColor("green");
		
		gof.createCircle().setColor("blue");
		
		Scene.instance.draw();
		Scene.instance.exportToJSON();
	}

}
