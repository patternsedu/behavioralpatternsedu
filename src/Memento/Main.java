package Memento;

import Memento.graph.AbstractGOFactory;
import Memento.graph.ColorGOFactory;
import Memento.graph.Scene;
import Memento.graph.Scene.SceneSnapshot;

public class Main {

	public static void main(String[] args) {
		// Построение исходной сцены
		AbstractGOFactory gof = new ColorGOFactory();
		gof.createPoint().setColor("red");
		gof.createPoint().setColor("green");
		System.out.println("Original scene");
		Scene.instance.draw();

		// Создаём первый снимок
		SceneSnapshot s1 = Scene.instance.save();

		// Изменяем состояние сцены
		System.out.println("Black scene");
		Scene.instance.setColor("black");
		Scene.instance.draw();

		// Востонавливаем состояние сцены
		System.out.println("Restored scene");
		Scene.instance.restore(s1);
		Scene.instance.draw();
	}

}
