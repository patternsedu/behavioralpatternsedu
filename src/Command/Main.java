package Command;

import Command.commands.PrintCommand;
import Command.commands.VerifyCommand;
import Command.widgets.ActionButton;
import Iterator.widgets.CompositeControl;
import Iterator.widgets.Label;
import Iterator.widgets.MainWindow;

public class Main {

	public static void main(String[] args) {

		CompositeControl mainWin = new MainWindow();
		CompositeControl frame1 = new CompositeControl()
				.add(new Label("Login")).add(new ActionButton("OK"));

		CompositeControl frame2 = new CompositeControl().add(new Label("Password"));
		ActionButton verifyButton = new ActionButton("Verify");
		verifyButton.setCommand(new VerifyCommand());
		frame2.add(verifyButton);

		CompositeControl frame3 = new CompositeControl().add(new Label("Print"));
		ActionButton printButton = new ActionButton("Print");
		printButton.setCommand(new PrintCommand("Epson", frame1));
		frame3.add(printButton);

		mainWin.add(frame1).add(frame2).add(frame3);
		mainWin.draw();
		
		verifyButton.press(); // Команда Verify
		printButton.press();  // Команда Print
	}
}
