package Command.widgets;

import Command.commands.Command;
import Command.commands.CommandInvoker;
import Iterator.widgets.Button;

public class ActionButton extends Button implements CommandInvoker {

	private Command pressCommand;
	
	public ActionButton() {
		super();
	}
	
	public ActionButton(String text) {
		super(text);
	}

	public void press() {
		System.out.printf("Button %s pressed\n", getText());
		executeCommand();
	}

	@Override
	public void setCommand(Command command) {
		pressCommand = command;
	}

	@Override
	public void executeCommand() {
		if (pressCommand != null)
			pressCommand.execute();
		
	}
}
