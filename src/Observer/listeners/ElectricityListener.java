package Observer.listeners;

@FunctionalInterface
public interface ElectricityListener
{
	void electricityOn(Object source); 
}
