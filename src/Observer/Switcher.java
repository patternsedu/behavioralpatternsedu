package Observer;

import Observer.listeners.ElectricityListener;

import java.util.List;
import java.util.ArrayList;

public class Switcher {
	private final List<ElectricityListener> listeners = new ArrayList<>();
	
	public void addElectricityListener(ElectricityListener l)
	{
		listeners.add(l);
	}
	
	public void removeElectricityListener(ElectricityListener l)
	{
		listeners.remove(l);
	}

	protected void notifyListeners() {
		for(ElectricityListener l : listeners)
			l.electricityOn(this);
	}
	
	public void switchOn()
	{
		System.out.println("выключатель включён");
		notifyListeners();
	}
}
