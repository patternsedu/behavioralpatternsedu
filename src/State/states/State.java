package State.states;

import State.documents.Document;

public abstract class State {
	
	protected Document doc;
	
	public State(Document doc) {
		this.doc = doc;
	}
	
	public void onEnterState(State oldState) {
		if (oldState != null)
			System.out.printf("%s -> %s\n", oldState, this);
	}

	// actions
	public void verify() { }
	public void approve() {}
	public void deny() {}
}
