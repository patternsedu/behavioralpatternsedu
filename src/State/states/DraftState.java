package State.states;

import State.documents.Document;

public class DraftState extends State {
	public DraftState(Document doc) {
		super(doc);
	}

	@Override
	public void verify() {
		if (this.doc.getSumma() > 0)
			doc.changeState(new ReviewState(doc));
	}

	@Override
	public String toString() {
		return "Draft";
	}
}
