package State;

import State.documents.Invoice;

public class Main {

	public static void main(String[] args) {
		Invoice inv1 = new Invoice(500, "Рога");
		inv1.verify();
		inv1.approve();
		System.out.println(inv1);

		Invoice inv2 = new Invoice(2500, "Копыта");
		inv2.verify();
		inv2.deny();
		System.out.println(inv2);

		Invoice inv3 = new Invoice(0, "Рога&Копыта");
		System.out.println(inv3);
	}
}
