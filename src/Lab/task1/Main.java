package Lab.task1;

import Lab.task1.widgets.Button;
import Lab.task1.widgets.CompositeControl;
import Lab.task1.widgets.Label;
import Lab.task1.widgets.MainWindow;

public class Main {

	public static void main(String[] args) {
		
		CompositeControl mainWin = new MainWindow();
		CompositeControl frame1 = new CompositeControl()
				.add(new Label("Login")).add(new Button("OK"));
		CompositeControl frame2 = new CompositeControl()
				.add(new Label("Password")).add(new Button("Verify"));
		Button printButton = new Button("Print");
		mainWin.add(frame1).add(frame2).add(new CompositeControl().add(printButton));

		printButton.addListener((h, e) -> System.out.println("> Button monitor: pressed!"));
		mainWin.addListener((h, e) -> System.out.println("> Main window monitor: pressed!"));
		
		// отрисовка окна
		mainWin.draw();
		printButton.press();
	}
}
