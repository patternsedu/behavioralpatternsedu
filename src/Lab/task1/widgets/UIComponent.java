package Lab.task1.widgets;

import Lab.task1.events.EventListener;
import Lab.task1.events.EventRequest;
import Lab.task1.events.Handler;

import java.util.ArrayList;
import java.util.List;

// Будет также выступать в роли BaseHandler
public abstract class UIComponent implements Handler {

	List<EventListener> listeners = new ArrayList<>();
	
	private Handler nextHandler;
	
	public abstract boolean draw(int line);
	public abstract int getHeight();
	public abstract int getWidth();

	public void addListener(EventListener listener) {
		this.listeners.add(listener);
	}
	
	@Override
	public void addNextHandler(Handler next) {
		nextHandler = next;
	}
	
	@Override
	public void handle(EventRequest request) {
		if (request.isHandled()) return; // Прерывание
		System.out.printf("Handle event in %s\n", this);
		listeners.forEach(l -> l.notify(this, request));
		if (nextHandler != null)
			nextHandler.handle(request);
	}

}
