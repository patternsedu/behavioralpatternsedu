package Lab.task1.events;

@FunctionalInterface
public interface EventListener {
    void notify(Object object, EventRequest eventRequest);
}
