package Lab.task1.events;

public interface Handler {
	void addNextHandler(Handler next);
	void handle(EventRequest request);
}
