package Lab.task2.exports;

import Lab.task2.graph.Circle;
import Lab.task2.graph.Point;

public class XMLExportVisitor implements ExportVisitor {

	@Override
	public void exportPoint(Point p) {
		System.out.printf("<point x=\"%d\", y=\"%d\", color=\"%s\" />",
				p.getX(), p.getY(), p.getColor());
	}

	@Override
	public void exportCircle(Circle c) {
		System.out.printf("<circle cx=\"%d\", cy=\"%d\", radius=\"%d\", color=\"%s\" />",
				c.getX(), c.getY(), c.getR(), c.getColor());
	}

}
