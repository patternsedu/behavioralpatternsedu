package Lab.task2.exports;

public interface Exportable {
	void accept(ExportVisitor v);
}
