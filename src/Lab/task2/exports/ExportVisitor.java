package Lab.task2.exports;

import Lab.task2.graph.Circle;
import Lab.task2.graph.Point;

public interface ExportVisitor {
	void exportPoint(Point p);
	void exportCircle(Circle c);
}
