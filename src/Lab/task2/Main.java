package Lab.task2;

import Lab.task2.graph.AbstractGOFactory;
import Lab.task2.graph.ColorGOFactory;
import Lab.task2.graph.Scene;

public class Main {

	public static void main(String[] args) {
		AbstractGOFactory gof = new ColorGOFactory();
		gof.createPoint().setColor("red");
		gof.createPoint().setColor("green");
		gof.createCircle().setColor("blue");
		
		Scene.instance.draw();
		Scene.instance.exportToJSON();
		Scene.instance.exportToXML();
	}

}
