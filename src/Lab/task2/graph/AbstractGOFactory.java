package Lab.task2.graph;

public abstract  class AbstractGOFactory {
	public abstract Point createPoint();
	
	public abstract Circle createCircle();
}
