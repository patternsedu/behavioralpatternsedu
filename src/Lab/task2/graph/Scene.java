package Lab.task2.graph;

import Lab.task2.exports.ExportVisitor;
import Lab.task2.exports.Exportable;
import Lab.task2.exports.JSONExportVisitor;
import Lab.task2.exports.XMLExportVisitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Scene {
	private List<GraphObject> objects ;
	public static final Scene instance = new Scene();
	
	private Scene() {
		objects = new ArrayList<>();
	}
	
	public void add(GraphObject o) {
		objects.add(o);
	} 
	
	public void clear() {
		objects.clear();
	}
	
	public void draw() {
		for(GraphObject g : objects)
			g.draw();
	}
	
	public void exportToJSON() {
		ExportVisitor visitor = new JSONExportVisitor();
		Iterator<GraphObject> iter = objects.iterator();
		System.out.print("[\n");
		while (iter.hasNext()) {
			GraphObject g = iter.next();
			if (g instanceof Exportable) {
				System.out.print('\t');
				((Exportable)g).accept(visitor);
				if (iter.hasNext()) System.out.println(',');
			}
		}
		System.out.println("\n]");
	}

	public void exportToXML() {
		ExportVisitor visitor = new XMLExportVisitor();
		Iterator<GraphObject> iter = objects.iterator();
		System.out.println("<scene>");
		while (iter.hasNext()) {
			GraphObject g = iter.next();
			if (g instanceof Exportable) {
				System.out.print('\t');
				((Exportable)g).accept(visitor);
				if (iter.hasNext()) System.out.println(',');
			}
		}
		System.out.println("\n</scene>");
	}
}
