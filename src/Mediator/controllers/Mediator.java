package Mediator.controllers;

import Mediator.widgets.UIComponent;

public interface Mediator {
	void notify(UIComponent sender, String event);
}
