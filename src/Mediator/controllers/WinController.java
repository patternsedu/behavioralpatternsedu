package Mediator.controllers;

import Mediator.tests.User;
import Mediator.widgets.Button;
import Mediator.widgets.CompositeControl;
import Mediator.widgets.Label;
import Mediator.widgets.MainWindow;
import Mediator.widgets.UIComponent;

public class WinController implements Mediator {
	private CompositeControl mainWin;
	private Button printButton; 
	
	public CompositeControl generateWindow()
	{
		mainWin = new MainWindow();

		CompositeControl frame1 = new CompositeControl()
				.add(new Label("Login")).add(new Button("OK"));

		User.verifyButton = new Button("Verify"); // для тестирования нажимания кнопки
		CompositeControl frame2 = new CompositeControl()
				.add(new Label("Password")).add(User.verifyButton);
		
		printButton = new Button("Print");
		printButton.setVisible(false);
		
		mainWin.add(frame1).add(frame2).
				add(new CompositeControl().add(printButton));
		
		mainWin.setController(this);
		
		return mainWin;
	}

	@Override
	public void notify(UIComponent sender, String event) {
		if (event.equals("Verify")) {
			printButton.setVisible(true);
			mainWin.draw();
		}
	}
}
