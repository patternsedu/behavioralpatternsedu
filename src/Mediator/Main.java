package Mediator;

import Mediator.controllers.WinController;
import static Mediator.tests.User.verifyButton;

public class Main {

	public static void main(String[] args) {
		new WinController().generateWindow().draw();
		verifyButton.press(); // имитация нажатия
	}
}
