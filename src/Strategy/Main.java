package Strategy;

import Strategy.strategies.MaxStrategy;
import Strategy.strategies.MinStrategy;
import Strategy.strategies.RandomStrategy;

public class Main {

	public static void main(String[] args) {
		Context ctx = new Context(10, 20, 5, 30, 17, 47, 28, 36);
		ctx.setStrategy(new MinStrategy()).execute();
		ctx.setStrategy(new MaxStrategy()).execute();
		ctx.setStrategy(new RandomStrategy()).execute();
	}

}
