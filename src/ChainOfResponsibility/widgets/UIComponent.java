package ChainOfResponsibility.widgets;

import ChainOfResponsibility.events.EventRequest;
import ChainOfResponsibility.events.Handler;

// Будет также выступать в роли BaseHandler
public abstract class UIComponent implements Handler{
	
	private Handler nextHandler;
	
	public abstract boolean draw(int line);
	public abstract int getHeight();
	public abstract int getWidth();
	
	@Override
	public void setNextHandler(Handler next) {
		nextHandler = next;
	}
	
	@Override
	public void handle(EventRequest request) {
		if (request.isHandled()) return; // Прерывание
		System.out.printf("Handle event in %s\n", this);
		if (nextHandler != null)
			nextHandler.handle(request);
	}

}
